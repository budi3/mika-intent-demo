package id.mikaapp.mikaintentdemo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import id.mikaapp.mikaintentdemo.MainMenuActivity.Companion.MIKA_INTENT_REQUEST_CODE
import kotlinx.android.synthetic.main.activity_transaction.*

class TransactionActivity : BaseChildActivity(R.layout.activity_transaction) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainMenuActivity.username?.let { username.setText(it) }
        MainMenuActivity.password?.let { password.setText(it) }
        createTransaction.setOnClickListener {
            try {
                val intent = Intent("id.mikaapp.mika.charge").apply {
                    putExtra("USERNAME", username.text.toString())
                    putExtra("PASSWORD", password.text.toString())
                    putExtra("AMOUNT", amount.text.toString().toIntOrNull() ?: 0)
                    acquirerId.text.toString().let {
                        if (it.isNotBlank()) putExtra(
                            "ACQUIRER_ID",
                            acquirerId.text.toString()
                        )
                    }
                    // DEBUG
                    // Create
                    debugCreateDelay.text.toString().let {
                        if (it.isNotBlank()) putExtra(
                            "DEBUG_TRANSACTION_CREATE_DELAY",
                            it.toInt()
                        )
                    }
                    putExtra(
                        "DEBUG_TRANSACTION_CREATE_NO_RESPONSE",
                        debugCreateNoResponse.isChecked
                    )
                    debugCreateResponseCode.text.toString().let {
                        if (it.isNotBlank()) putExtra(
                            "DEBUG_TRANSACTION_CREATE_RESPONSE_CODE",
                            it
                        )
                    }
                    // Void
                    debugVoidDelay.text.toString().let {
                        if (it.isNotBlank()) putExtra(
                            "DEBUG_TRANSACTION_VOID_DELAY",
                            it.toInt()
                        )
                    }
                    putExtra(
                        "DEBUG_TRANSACTION_VOID_NO_RESPONSE",
                        debugVoidNoResponse.isChecked
                    )
                    debugVoidResponseCode.text.toString().let {
                        if (it.isNotBlank()) putExtra(
                            "DEBUG_TRANSACTION_VOID_RESPONSE_CODE",
                            it
                        )
                    }
                    // Reverse
                    debugReverseDelay.text.toString().let {
                        if (it.isNotBlank()) putExtra(
                            "DEBUG_TRANSACTION_REVERSE_DELAY",
                            it.toInt()
                        )
                    }
                    putExtra(
                        "DEBUG_TRANSACTION_REVERSE_NO_RESPONSE",
                        debugReverseNoResponse.isChecked
                    )
                    debugReverseResponseCode.text.toString().let {
                        if (it.isNotBlank()) putExtra(
                            "DEBUG_TRANSACTION_REVERSE_RESPONSE_CODE",
                            it
                        )
                    }
                    // Reverse Void
                    debugReverseVoidDelay.text.toString().let {
                        if (it.isNotBlank()) putExtra(
                            "DEBUG_TRANSACTION_REVERSE_VOID_DELAY",
                            it.toInt()
                        )
                    }
                    putExtra(
                        "DEBUG_TRANSACTION_REVERSE_VOID_NO_RESPONSE",
                        debugReverseVoidNoResponse.isChecked
                    )
                    debugReverseVoidResponseCode.text.toString().let {
                        if (it.isNotBlank()) putExtra(
                            "DEBUG_TRANSACTION_REVERSE_VOID_RESPONSE_CODE",
                            it
                        )
                    }
                }
                startActivityForResult(intent, MIKA_INTENT_REQUEST_CODE)
            } catch (e: Exception) {
                Toast.makeText(this, e.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MIKA_INTENT_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                requireNotNull(data)
                val transactionId = data.getStringExtra("TRANSACTION_ID")
                val approvalCode = data.getStringExtra("APPROVAL_CODE")
                val firstSixLastFourPan = data.getStringExtra("FIRST_SIX_LAST_FOUR_PAN")
                MainMenuActivity.lastSuccessTransactionId = transactionId
                Toast.makeText(
                    this,
                    "TRANSACTION SUCCESS\n" +
                            "Transaction Id: $transactionId\n" +
                            "Approval Code: $approvalCode\n" +
                            "Masked Pan: ${firstSixLastFourPan?.replace("*", "******")}",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (resultCode == Activity.RESULT_CANCELED) {
                val errorCode = data?.getStringExtra("ERROR_CODE")
                val errorMessage = data?.getStringExtra("ERROR_MESSAGE")
                Toast.makeText(
                    this,
                    "TRANSACTION FAILED\n $errorCode: $errorMessage",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}